const fs = require('fs')

const WasmFunctionProcess = require('./WasmFunctionProcess').WasmFunctionProcess

/* [DOC]
## class WasmExecutor

- a new instance of the wasm executor as one new function process

[DOC] */
class WasmExecutor {
  functionProcesses = []
  wasmFileName = null

  //TODO: version
  name = null
  version = null
  isDefaultVersion = null
  wasmFunctionsFolder = null

  constructor(wasmFileName, wasmFunctionsFolder, isDefaultVersion=false) {
    this.wasmFileName = wasmFileName
    this.functionProcesses.push(new WasmFunctionProcess())
    //TODO: handle bad naming
    let tokens = this.wasmFileName.split('_v_')
    this.name = tokens[0]
    this.version = tokens[1].split('.wasm')[0]
    //TODO: save somewhere the list of versions and default version
    //TODO: first do something in a file
    this.isDefaultVersion = isDefaultVersion
    this.wasmFunctionsFolder = wasmFunctionsFolder
  }

  activateAsDefaultVersion() {
    this.isDefaultVersion = true
  }

  deActivateAsDefaultVersion() {
    this.isDefaultVersion = false
  }

  addWasmFunctionProcess(wasmFunctionProcess) {
    this.functionProcesses.push(wasmFunctionProcess)
    return wasmFunctionProcess
  }

  addNewWasmFunctionProcess() {
    let newWasmFunctionProcess = new WasmFunctionProcess()
    this.functionProcesses.push(newWasmFunctionProcess)
    return newWasmFunctionProcess
  }

  giveMeTheMostAppropriateFunctionProcess() {
    const rndInt = Math.floor(Math.random() * this.functionProcesses.length) + 1
    let index = rndInt - 1
    return this.functionProcesses[index]
  }


/* [DOC]
### function: `killWasmFunctionProcess`

This function allows to kill a specific process of the wasm executor

> 🖐 processIndex is not pid, it's the id of process in the array of functionProcesses

**Parameters**:
- `processIndex` (Integer): *the index of the process*

**Return**:
- JavaScript object (Json payload)

[DOC] */
  killWasmFunctionProcess(processIndex) {
    
    let wasmFunctionProcess = this.functionProcesses[processIndex]

    if(wasmFunctionProcess) { // the process exists

      let processToKill = wasmFunctionProcess.process
      let relatedWasmFunction = wasmFunctionProcess.wasmFunction
  
      try {
        processToKill.kill()
        this.functionProcesses.splice(processIndex,1)
        
        return {
          failure: null,
          success: {
            message: `${this.wasmFileName} process index: ${processIndex} killed`,
            action: "killed",
            status: "success"
          }
        }
  
      } catch(error) {
        relatedWasmFunction.status = "🔥 unable to kill"
        //return { error: error} 
        return {
          failure: error.message, success: null
        }
      }


    } else {
      return {
        failure: `${this.wasmFileName} process index: ${processIndex} doesn't exist`,
        success: null
      }
    } // end if
    

  }


/* [DOC]
### function: `killAllWasmFunctionProcesses`

[DOC] */
  killAllWasmFunctionProcesses() {
    let errors = []
    this.functionProcesses.forEach((wasmFunctionProcess, processIndex) => {
      let processToKill = wasmFunctionProcess.process
      let relatedWasmFunction = wasmFunctionProcess.wasmFunction
      try {
        processToKill.kill()
        this.functionProcesses.splice(processIndex,1)
      } catch (error) {
        relatedWasmFunction.status = "🔥 unable to kill" // mark the process
        errors.push(error)
      }
    }) // end forEach

    if(errors.length>0) {
      return { 
        failure: "errors when killing process", 
        errors: errors,
        success: null
      } // JSON.stringify ? 
    } else {
      return {
        failure: null,
        success: {
          message: `all ${this.wasmFileName} processes are killed`,
          action: "killed",
          status: "success"
        }
      }    
    }
  }

/* [DOC]
### function: `deleteWasmFile`

[DOC] */
  deleteWasmFile() { // promise
    return new Promise((resolve, reject) => {

      fs.unlink(`${this.wasmFunctionsFolder}/${this.wasmFileName}`, (error) => {
        if (error) {
          reject({
            success: null, failure: error.message
          })
        } else {
          resolve({
            success: {
              message: `${this.wasmFileName} deleted`
            }, 
            failure: null
          })
        }
      
      })
    })
  }

/* [DOC]
### function: `killWasmFunctionProcessByPid`

[DOC] */
  killWasmFunctionProcessByPid(pid) {
    // TODO:
  }
}

module.exports = {
  WasmExecutor
}