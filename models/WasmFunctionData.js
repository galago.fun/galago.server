/* [DOC]
## class WasmFunctionData

[DOC] */
class WasmFunctionData {
  status = "👀 waiting"
  started = null
  ended = null
  duration = null
  available = true
  constructor() {

  }
}

module.exports = {
  WasmFunctionData
}