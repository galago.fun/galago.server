#!/bin/bash
git clone --depth 1 -b main https://gitlab.com/galago.fun/galago.cli.git
cd galago.cli
GOOS=linux GOARCH=amd64 go build -o ../galago
cd ..
chmod +x galago
sudo cp galago /usr/local/bin
rm galago

export GALAGO_URL="http://localhost:8080"
export GALAGO_TOKEN="ILOVEPANDA"
