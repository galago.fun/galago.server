/*
### How to
```bash
node gen.doc.js issues.helper.js documentation
```
*/
const fs = require("fs")
const path = require("path")

let sourceCode = process.argv[2]
let target = process.argv[3]

let startToken = "/* [DOC]"
let endToken = "[DOC] */"

let content = fs.readFileSync(sourceCode).toString()
let rowsContent = content.split("\n")
let documentation = []

var isPartOfDoc = false
var docStart = false
rowsContent.forEach(row => {
  if(row.trim().startsWith(startToken)) { 
    isPartOfDoc = true
    docStart = true
  }
  if(row.trim().startsWith(endToken)) {
    isPartOfDoc = false
    documentation.push("")
  }
  if(isPartOfDoc && !docStart) {
    documentation.push(row)
  } else {
    docStart = false
  }
})

fs.writeFileSync(`${target}/${path.basename(sourceCode)}.md`, documentation.join("\n"))
