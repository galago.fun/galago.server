/* [DOC]
## wasm.child.processes.js

- `/functions/processes/list`
- `/function/kill/:function_name/:function_version/:process_index`
- `/functions/processes/table` 🖐 debug purpose
- `/functions/processes/data`
- `/functions/processes/data/:function_name`

[DOC] */

const functions = require('../helpers/wasm.functions')

async function wasmChildProcesses (fastify, options) {

  // route protection
  // see galagoctl.config
  // see galago.start.sh
  fastify.addHook('onRequest', async (request, reply) => {
    let token = request.headers["admin_galago_token"]
    if (options.adminGalagoToken==="" || options.adminGalagoToken===token) {
      // all good
    } else {
      reply.code(401).send({
        failure: "😡 Unauthorized",
        success: null
      })
    }
  })


/* [DOC]
### route `/functions/processes/list`

This route returns a json payload of the list of the wasm executors (and their related processes)

> How to use it
```bash
curl "${url_api}/functions/processes/list"
``` 
[DOC] */
  fastify.get(`/functions/processes/list`, async (request, reply) => {
    return Array.from(options.wasmExecutorsMap)
  })

/* [DOC]
### route `/processes/kill/:function_name/:executor_version/:process_index`

This route allows to kill a process of a wasm executor

> How to use it
```bash
function_name="hello"
function_version="first"
process_index=0
curl \
  -X POST "${url_api}/functions/kill/${function_name}/${function_version}/${process_index}"
``` 

> usefull to scale down a wasm executor or kill a long running function
[DOC] */
  fastify.post(`/functions/kill/:function_name/:function_version/:process_index`, async (request, reply) => {
    // TODO: remove emojis 😢

    let processIndex = request.params.process_index
    let functionName = request.params.function_name
    // executor version == function version
    let executorVersion = request.params.function_version

    let wasmExecutorEntry = options.wasmExecutorsMap.get(`${functionName}_v_${executorVersion}.wasm`) 

    if(wasmExecutorEntry) {
      return wasmExecutorEntry.killWasmFunctionProcess(processIndex)
    } else {
      return {
        failure: `😡 ${functionName}_v_${executorVersion} doesn't exist`,
        success: null
      }
    }
  })

/* [DOC]
### route `/functions/processes/table`

> 🚧 this route exists for debugging purpose
[DOC] */  
  fastify.get(`/functions/processes/table`, async (request, reply) => {

    let executorsCollection = Array.from(options.wasmExecutorsMap)
    let functionsList = functions.getList({executorsCollection})

    return functionsList.map(functionRecord => `${functionRecord.function}: ${functionRecord.version} ${functionRecord.isDefaultVersion} ${functionRecord.processIndex} ${functionRecord.pid}`).join("\n")

  })

/* [DOC]
### route `/functions/processes/data`

#### Call GalaGo API from outside GitPod

First, on the GitPod side, type this command `gp url 8080` to get the exposed url of the service (something like that: https://8080-ivory-hyena-vqy1nvtx.ws-eu11.gitpod.io)

> 🖐️ don't forget to expose the port and give it a public visibility (in .gitpod.yml)

#### Executors collection format

```javascript
[
  [
    'hello_v_000.wasm',
    {
      wasmFileName: 'hello_v_000.wasm',
      name: 'hello',
      version: '000',
      isDefaultVersion: false
    }
  ],
  [
    'hello_v_001.wasm',
    {
      wasmFileName: 'hello_v_001.wasm',
      name: 'hello',
      version: '001',
      isDefaultVersion: true
    }
  ]
]
```

[DOC] */  

  fastify.get(`/functions/processes/data`, async (request, reply) => {
    // list/array of all "executors"
    let executorsCollection = Array.from(options.wasmExecutorsMap)
    
    return functions.getList({executorsCollection})
  })

/* [DOC]
### route `/functions/processes/data/:function_name`

[DOC] */  
  fastify.get(`/functions/processes/data/:function_name`, async (request, reply) => {
    let functionName = request.params.function_name
    let executorsCollection = Array.from(options.wasmExecutorsMap)

    return functions.getList({
      executorsCollection: executorsCollection.filter(executor => executor[0].startsWith(`${functionName}_v_`))
    })

  })

}

module.exports = wasmChildProcesses
