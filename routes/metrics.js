/* [DOC]
## metrics.js

This route (`http://galago_domain_name/metrics`) allows to get Prometheus metrics.

### Prometheus metrics

A metric is composed by several fields:
- Metric name
- Any number of labels (can be 0), represented as a key-value array
- Current metric value
- Optional metric timestamp

Ref: 
- https://sysdig.com/blog/prometheus-metrics/
- https://gianarb.it/blog/prometheus-openmetrics-expositon-format

```
# HELP metric_name Description of the metric
# TYPE metric_name type
# Comment that's not parsed by prometheus
http_requests_total{method="post",code="400"}  3   1395066363000
```

### GalaGo Prometheus metrics

The exposed metric is a counter, it counts the number of call of a wasm function of a child process of a wasm executor.

#### Sample

```
# HELP hello_v_000_2829_count
# TYPE hello_v_000_2829_count counter
hello_v_000_2829 0
# HELP hello_v_001_2835_count
# TYPE hello_v_001_2835_count counter
hello_v_001_2835 0
```
[DOC] */

const path = require('path')

async function metrics (fastify, options) {

  fastify.get(`/metrics`, async (request, reply) => {

    let data = []

    // list of processes sets
    let processesSetsList = Array.from(options.wasmExecutorsMap)
    
    processesSetsList.forEach(row => {

      let wasmFileName = row[0]
      let relatedProcessesList = row[1].functionProcesses
      
      relatedProcessesList.forEach((item, index) => {
        let process = item.process
        let counter = item.counter
        let functionName = path.basename(wasmFileName, ".wasm")

        data.push(`# HELP ${functionName}_${process.pid}_count`)  
        data.push(`# TYPE ${functionName}_${process.pid}_count counter`)  
        data.push(`${functionName}_${process.pid} ${counter}`)  
        //data.push(``)  
      })
    })
    reply.type("text/plain; charset=utf-8")
    return data.join("\n")
    
  })

}

module.exports = metrics
