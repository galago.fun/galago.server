/* [DOC]
## wasm.publish.js

- `/functions/publish`

[DOC] */

const fs = require('fs')
const streamToFile = require('../helpers/stream.helper').streamToFile
const createWasmExecutor = require('../helpers/wasm.executor').createWasmExecutor

async function publish (fastify, options) {

  // route protection
  // see galagoctl.config
  // see galago.start.sh
  fastify.addHook('onRequest', async (request, reply) => {
    let token = request.headers["admin_galago_token"]
    if (options.adminGalagoToken==="" || options.adminGalagoToken===token) {
      // all good
    } else {
      reply.code(401).send({
        failure: "😡 Unauthorized",
        success: null
      })
    }
  })

/* [DOC]
### route: `/functions/publish`

This route allows to upload a wasmfile to the server. Then a wasm executor will be created to hande the new function.

> How to use it
```bash
function_name="hello"
wasm_file="./hello.wasm"
function_version="001"
curl -F "${function_name}=@${wasm_file}" \
      -H "Content-Type: multipart/form-data" \
      -X POST ${url_api}/functions/publish/${function_version}
``` 
[DOC] */
  fastify.post(`/functions/publish/:version`, async (request, reply) => {
    let version = request.params.version

    const data = await request.file()

    /* properties of data
      data.file // stream
      data.fields // other parsed parts
      data.fieldname
      data.filename
      data.encoding
      data.mimetype
    */
    
    // check the name
    let wasmFileName = `${data.fieldname}_v_${version}.wasm`
    
    // get the first process of thr wasm executor
    let wasmChildProcess = createWasmExecutor({
      wasmFileName: wasmFileName,
      wasmExecutorsMap: options.wasmExecutorsMap,
      wasmExecutorsVersionsStates: options.wasmExecutorsVersionsStates,
      wasmFunctionsFolder: options.wasmFunctionsFolder
    }).functionProcesses[0].process

    // Define actions on message
    wasmChildProcess.once("message", (message) => {
      console.log("🤖> loading handle function after building:", message)
      if(message.success) { 

        reply.send({
          failure: null,
          success: {
            message: "🚢 function deployed",
            action: "deployed",
            status: "success"
          }
        })

      } else {
        console.log("😡 message", message)
        if(message.failure) {
          reply.send({failure: message.failure, success: null})
        } else {
          reply.send({failure: "unknown error from wasm worker", success: null})
        }
      }

    }) // end of on message definition

    try {
      // Get the file from the stream
      const wasmFile = await streamToFile({isString:false, stream:data.file})
      // Write the file on disk
      fs.writeFileSync(`${options.wasmFunctionsFolder}/${data.fieldname}_v_${version}.wasm`, wasmFile)
      // Send message to the worker to:
      // - load the wasm file
      // - load the Handle function in the global scope of the worker
      /*
        📝 structure: {
          cmd: 'load',
          wasmFilePath: './wasm.functions/tinygo.index_v_0.0.0.wasm',
          compiler: 'tinygo',
          functionName: 'tinygo.index_v_0.0.0',
          wasmFileName: 'tinygo.index_v_0.0.0.wasm'
        }
      */

      wasmChildProcess.send({
        cmd: "load",
        wasmFilePath: `${options.wasmFunctionsFolder}/${data.fieldname}_v_${version}.wasm`,
        compiler: `${data.fieldname}_v_${version}`.split(".")[0],
        functionName: `${data.fieldname}_v_${version}`,
        wasmFileName: `${data.fieldname}_v_${version}.wasm`
      })
    } catch(error) {
      console.log("😡", error)
      reply.send({
        failure: "error when loading wasm file",
        success: null
      })
    }
  
    await reply

  })

}

module.exports = publish
