package main

import (
	"syscall/js"
)

/*
Build:
./galagoctl function build ./demo.redis.go ./redis.wasm

Deploy:
./galagoctl function deploy redis ./redis.wasm A
./galagoctl executor activate redis A

Call:
./galagoctl function call redis '{}'
*/
func redisSetCallBack(this js.Value, args []js.Value) interface{} {
	error := args[0].JSValue()
	result := args[1].String()

	if !error.IsNull() {
		println("😡 error: " + error.String())
	} else {
		println("🙂 result is " + result)
	}
	return ""
}

func Handle(_ js.Value, args []js.Value) interface{} {
	println("🌈 executing redis function")

	js.Global().Call("redisSet", "message", "hello", js.FuncOf(redisSetCallBack))

	redisGetCallBack := func(this js.Value, args []js.Value) interface{} {
		error := args[0].JSValue()
		result := args[1].String()

		if !error.IsNull() {
			println("😡 error: " + error.String())
		} else {
			println("🙂 result is " + result)
		}
		return ""
	}

	js.Global().Call("redisGet", "message", js.FuncOf(redisGetCallBack))

	return map[string]interface{}{
		"author": "@k33g 🐼",
	}
}

func main() {
	println("🤩 I ❤️ GalaGo")

	redisConnectedCallBack := func(this js.Value, args []js.Value) interface{} {
		wasmFunctionName := args[0].String()
		println("➡️ " + wasmFunctionName + " redis client connected")
		return ""
	}

	js.Global().Call("redisInitialize", js.FuncOf(redisConnectedCallBack))


	js.Global().Set("Handle", js.FuncOf(Handle))
	<-make(chan bool)
}
