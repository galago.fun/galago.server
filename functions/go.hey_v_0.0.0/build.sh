#!/bin/bash
#tinygo build -o hello.wasm -target wasm ./main.go
#tinygo build -o ../../wasm.functions/hello_v_0.0.2.wasm -target wasm ./main.go

GOOS=js GOARCH=wasm go build -o go.hey.wasm ./main.go
#GOOS=js GOARCH=wasm go build -o ../../wasm.functions/go.hey_v_0.0.0.wasm ./main.go
galago -cmd=deploy -name=go.hey -wasm=./go.hey.wasm -ver=0.0.0
galago -cmd=activate -name=go.hey -ver=0.0.0
galago -cmd=data
galago -cmd=call -name=go.hey -json='{"name":"Bob Morane"}'

ls -lh *.wasm
