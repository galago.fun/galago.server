package main

import (
	"syscall/js"
)

func Html(_ js.Value, args []js.Value) interface{} {
	return `
		<h1>🎃 Hello World 🎃</h1>
		<h2>Tiny GoLang is amazing</h2>
	`
}

func main() {

	js.Global().Set("Html", js.FuncOf(Html))

	<-make(chan bool)
}
// https://8080-orange-silverfish-67x91xes.ws-eu17.gitpod.io/functions/web/html/index/0.0.0