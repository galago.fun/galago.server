#!/bin/bash
#tinygo build -o hello.wasm -target wasm ./main.go
#tinygo build -o ../../wasm.functions/hello_v_0.0.2.wasm -target wasm ./main.go

GOOS=js GOARCH=wasm go build -o go.index.wasm ./main.go
GOOS=js GOARCH=wasm go build -o ../../wasm.functions/go.index_v_0.0.0.wasm ./main.go

ls -lh *.wasm
