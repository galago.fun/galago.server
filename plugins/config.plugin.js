/* [DOC]
## Config plugin

### function getConfig

- Return a JavaScript object
- Usage (from Go): `js.Global().Call("getSettings", []interface{}{}).JSValue().Get("field_name")`

[DOC] */
const config = require("../galago.config").config

function plugin(global) {

  function getConfig(options) {
    return config
  }
  global.getConfig = getConfig

}

module.exports = {
  plugin
}