/* [DOC]
## GalaGo Plugins

> - 🚧 This is a work in progress (== things may be change)
> - TODO: create a pluginlist in a file

### function implantPlugins

[DOC] */
function implantPlugins(global) {

  require("./config.plugin").plugin(global)
  require("./environment.plugin").plugin(global)
  require("./redis.plugin").plugin(global)
  require("./mqtt.plugin").plugin(global)

}

module.exports = {
  implantPlugins
}