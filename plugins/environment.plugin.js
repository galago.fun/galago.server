/* [DOC]
## Environment variable plugin

### function getEnvironmentVariable

- Return the value of an environment variable
- If the name of the variable starts with `SECRET_`, it returns `**********`
- Usage (from Go): example: `galagoVersion := js.Global().Call("getEnvironmentVariable", "GALAGO_VER").JSValue().String()`

[DOC] */
function plugin(global) {

  function getEnvironmentVariable(name) {
    // hide secret varibales
    if(name.startsWith("SECRET_")) {
      return "**********"
    } else {
      return process.env[name]
    }
    
  }
  global.getEnvironmentVariable = getEnvironmentVariable

}

module.exports = {
  plugin
}