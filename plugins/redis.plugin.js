/* [DOC]
## Redis Plugin

> 🚧 This is a work in progress (== things may be change)

[DOC] */
const redis = require("redis")

function plugin(global) {
  global.wasmRedisClient = null

/* [DOC]
### function redisInitialize

[DOC] */
  function redisInitialize(callback) {
    // Create the Redis client
    global.wasmRedisClient = redis.createClient(process.env["REDIS_PORT"], process.env["REDIS_ADDR"])
    /*
    wasmRedisClient.on("error", (error) => {
      console.error(error)
    })
    */
    global.wasmRedisClient.on('connect', _ => {
      console.log(`🪣> wasm redis client for ${global.wasmFunctionName} connected`)
      callback(global.wasmFunctionName)
    })

  }
  global.redisInitialize = redisInitialize

/* [DOC]
### function redisSet


[DOC] */
  function redisSet(key, value, callback) {
    global.wasmRedisClient.set(`${global.wasmFunctionName}:${key}`, value, (error, reply) => {
      callback(error, reply)
    })  
  }
  global.redisSet = redisSet

/* [DOC]
### function redisGet


[DOC] */
  function redisGet(key, callback) {
    global.wasmRedisClient.get(`${global.wasmFunctionName}:${key}`, (error, reply) => {
      callback(error, reply)
    })
  }
  global.redisGet = redisGet

}

module.exports = {
  plugin
}