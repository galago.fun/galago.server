/* [DOC]
## galago.server.js


[DOC] */
const path = require('path')
const fs = require('fs')

var httpPort = null
// export LOGGING=true; ./galago.start.sh
let fastifyServerOptions = {
  logger: process.env.LOGGING || true
}

/* [DOC]
### https

Define the below environment variables to activate ssl:
```bash
export GALGO_CERT="app.galago.fun.crt"
export GALGO_KEY="app.galago.fun.key"
export HTTPS_PORT="3000"
```
> **Remark**: the certificate and the key must be copied to the `./certs` directory

🖐 you should handle the ssl termination with NGINX or similar, handle it inside Node is very expensive in terms of performances.

[DOC] */
if(process.env.GALGO_CERT && process.env.GALGO_KEY) {
  fastifyServerOptions.https = {
    cert: fs.readFileSync(path.join(`${__dirname}/certs`, process.env.GALGO_CERT)),
    key: fs.readFileSync(path.join(`${__dirname}/certs`, process.env.GALGO_KEY))
  }
  httpPort = process.env.HTTPS_PORT || 3000
} else {
  httpPort = process.env.HTTP_PORT || 8080
}

const fastify = require('fastify')(fastifyServerOptions)

const WasmExecutorsVersionsStates = require('./models/WasmExecutorsVersionsStates').WasmExecutorsVersionsStates

const createWasmExecutor = require('./helpers/wasm.executor').createWasmExecutor

// folder where to store wasm file
const wasmFunctionsFolder = process.env.GALAGO_FUNCTIONS_PATH || './wasm.functions'
// folder where to store the posted GoLang files
const wasmFunctionsPostedSourceCodeFolder = process.env.GALAGO_FUNCTIONS_CODE || './functions.src'

const adminGalagoToken = process.env.ADMIN_GALAGO_TOKEN || ""

fastify.register(require('fastify-formbody'))
fastify.register(require('fastify-multipart'))

function createWasmExecutors({wasmFilesList, wasmExecutorsMap, wasmExecutorsVersionsStates, wasmFunctionsFolder}) {
  wasmFilesList.forEach(async wasmFileName => {
    
    // get the first process of the wasmExecutor
    let wasmChildProcess = createWasmExecutor({
      wasmFileName: wasmFileName,
      wasmExecutorsMap: wasmExecutorsMap,
      wasmExecutorsVersionsStates: wasmExecutorsVersionsStates,
      wasmFunctionsFolder: wasmFunctionsFolder
    }).functionProcesses[0].process

    let fileNameWithoutExtension = path.basename(wasmFileName, ".wasm")

    /*
      📝 structure: {
        cmd: 'load',
        wasmFilePath: './wasm.functions/tinygo.index_v_0.0.0.wasm',
        compiler: 'tinygo',
        functionName: 'tinygo.index_v_0.0.0',
        wasmFileName: 'tinygo.index_v_0.0.0.wasm'
      }
    */

    wasmChildProcess.send({
      cmd: "load",
      wasmFilePath: `${wasmFunctionsFolder}/${wasmFileName}`,
      compiler: fileNameWithoutExtension.split(".")[0],
      functionName: fileNameWithoutExtension,
      wasmFileName: wasmFileName
    })
  })
}

function registerRoutes({routesOptions}) {

  fastify.register(require('./routes/wasm.activate.js'), routesOptions)

  fastify.register(require('./routes/wasm.functions'), routesOptions)
  fastify.register(require('./routes/wasm.web.functions'), routesOptions)

  // this route allows publish a wasm function from a posted wasm file
  fastify.register(require('./routes/wasm.publish'), routesOptions)

  fastify.register(require('./routes/wasm.child.processes.js'), routesOptions)

  fastify.register(require('./routes/wasm.remove.js'), routesOptions)

  fastify.register(require('./routes/wasm.scale.js'), routesOptions)
  fastify.register(require('./routes/metrics.js'), routesOptions)    

}

// get the list of the wasm files
fs.readdir(wasmFunctionsFolder, (error, files) => {
  if(error) {
    console.log("😡 when listing the wasm files", error)
    process.exit(1)
  } else {
    let wasmFilesList = files.filter(file=>path.extname(file)==='.wasm')
    //fastify.log.info(`🤖> wasm files: ${wasmFilesList}`)
    let wasmExecutorsMap = new Map()
    
    let wasmExecutorsVersionsStates = new WasmExecutorsVersionsStates()
    
    
    createWasmExecutors({wasmFilesList, wasmExecutorsMap, wasmExecutorsVersionsStates, wasmFunctionsFolder})

    let routesOptions = {
      wasmExecutorsMap: wasmExecutorsMap,
      wasmExecutorsVersionsStates: wasmExecutorsVersionsStates,
      wasmFunctionsFolder: wasmFunctionsFolder,
      wasmFunctionsPostedSourceCodeFolder: wasmFunctionsPostedSourceCodeFolder,
      adminGalagoToken: adminGalagoToken
    }
    registerRoutes({routesOptions})

    // Serve the static assets
    fastify.register(require('fastify-static'), {
      root: path.join(__dirname, 'public'),
      prefix: '/'
    })


    // Run the server!
    const start = async _ => {
      try {
        await fastify.listen(httpPort, "0.0.0.0")
        fastify.log.info(`server listening on ${fastify.server.address().port}`)
      } catch (error) {
        fastify.log.error(error)
      }
    }
    start()

  }
})
