/* [DOC]
## wasm.executor.js

A **Wasm Executor** is an object that contains:
- a set (array) of nodejs child processes (1 to n, each bootstraping the same Go function)
- the status of every process (about the related Go function)
- a wasmExecutor is linked to only one wasm file (or execute only one function)

[DOC] */
const WasmExecutor = require('../models/WasmExecutor').WasmExecutor

/* [DOC]
### function: `createWasmExecutor`

This function is used when loading a wasm file:
- it adds an entry to the wasm executors map
- it creates a **first** nodejs child process
- it returns this first child process

**Parameters**:
- `wasmFileName` (String)
- `wasmExecutorsMap` (Map)

**Return**:
-  🚧 a wasm executor object

```javascript

```

[DOC] */
function createWasmExecutor({
  wasmFileName, wasmExecutorsMap, wasmExecutorsVersionsStates, wasmFunctionsFolder
}) {
  //🖐 TODO: if the worker already exists, you need to kill it before

  let newWasmExecutor = new WasmExecutor(wasmFileName, wasmFunctionsFolder)

  wasmExecutorsMap.set(
    wasmFileName, 
    newWasmExecutor
  )
  
  // set the default version
  // this will be the version called officially
  // lile the "deployed" version
  // the other versions remain reachable
  if(wasmExecutorsVersionsStates.states[newWasmExecutor.name]) {
    if(wasmExecutorsVersionsStates.states[newWasmExecutor.name] === newWasmExecutor.version) {
      newWasmExecutor.activateAsDefaultVersion()
    }
  } 

  return newWasmExecutor
}

/* [DOC]
### function: `addChildProcessToWasmExecutor`

This function allows to scale a wasm executor by adding a new process to the pool of child processes

**Parameters**:
- `wasmFileName` (String)
- `wasmExecutorsMap` (Map)

**Return**:
- WasmFunctionProcess

[DOC] */
function addNewWasmFunctionProcessToWasmExecutor({
  wasmFileName, wasmExecutorsMap
}) {

  try {
    // Add an entry to the array of processes
    let newWasmFunctionProcess = wasmExecutorsMap.get(wasmFileName).addNewWasmFunctionProcess()
    
    return newWasmFunctionProcess
  } catch(error) {
    throw error
  }


}

module.exports = {
  createWasmExecutor,
  addNewWasmFunctionProcessToWasmExecutor
}
