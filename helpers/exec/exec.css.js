function execCss(message, process) {
  try {
    let result = Css(message.funtionParameters, message.functionHeaders)
    process.send({success: result})
  } catch(error) {
    process.send({failure: error})
    throw error // 🤔
  }
}

exports.execCss = execCss