# bush.baby

> 🚧 (wip) this is a POC

GalaGo is a small FaaS platform hosting and running TinyGo or Go Wasm functions.

[![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/galago.fun/galago.server)

## Use Case

- Serving wasm Golang functions
- Calling function with http request
- Functions can communicate with MQTT
- Functions can use Redis

## Requirements and Install

### Requirements

- Install **Go** `version 1.16` with **gvm**
  ```bash
  # requirements
  # sudo apt-get install curl git mercurial make binutils bison gcc build-essential

  bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
  gvm install go1.16 -B
  gvm use go1.16
  go get -v -t -d ./...
  ```
- Install **TinyGo** `version 0.19.0`
  ```bash
  wget https://github.com/tinygo-org/tinygo/releases/download/v0.19.0/tinygo_0.19.0_amd64.deb
  sudo dpkg -i tinygo_0.19.0_amd64.deb
  rm tinygo_0.19.0_amd64.deb
  export PATH=$PATH:/usr/local/tinygo/bin
  tinygo version
  ```
- Install **NodeJS** `version 14.17.4`
- Install **Redis**
  ```bash
  sudo apt-get update
  sudo apt-get install -y redis-server
  sudo rm -rf /var/lib/apt/lists/*
  ```
- Install **PM2**
  ```bash
  npm install pm2@latest -g
  ```

### Install GalaGo server

- git clone this repository: `git clone --depth 1 -b main https://gitlab.com/galago.fun/galago.server.git`
  - or `git clone --depth 1 -b main git@gitlab.com:galago.fun/galago.server.git`
- run `npm install`

### Update

> WIP 🚧

## Start Galago server

```bash
export REDIS_ADDR="localhost"
export REDIS_PORT="6379"
export MQTT_ADDR="mqtt://localhost:1883"
export HTTP_PORT="8080"
export ADMIN_GALAGO_TOKEN="ILOVEPANDA"
export GALAGO_FUNCTIONS_PATH="./wasm.functions"
export GALAGO_FUNCTIONS_CODE="./functions.src"
# uncomment the 3 below lines to activate ssl
#export GALGO_CERT="app.galago.fun.crt"
#export GALGO_KEY="app.galago.fun.key"
#export HTTPS_PORT="3000"
redis-server --daemonize yes

pm2 start mqtt.broker.js
pm2 start galago.server.js
```

> - 🖐 you should handle the ssl termination with NGINX (or similar), handle it inside Node is very expensive in terms of performances.
> - 📝 use `pm2 logs` to watch the log of the applications
> - ℹ️ more information about **PM2**: [https://pm2.keymetrics.io/docs/usage/quick-start/](https://pm2.keymetrics.io/docs/usage/quick-start/)

## Create a function

### If TinyGo

- Create a `main.go` file:

```golang
package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	parameters := args[0]
	a := parameters.Get("a").Int()
	b := parameters.Get("b").Int()
	return a+b
}

func main() {
	println("🤖: add wasm loaded")
	js.Global().Set("Handle", js.FuncOf(Handle))
	<-make(chan bool)
}
```

### If Go

Change the `main` function like this:

```golang
func main() {
	println("🤖: add wasm loaded")

  go func(){ 
    js.Global().Call("startCb")
  }()

	js.Global().Set("Handle", js.FuncOf(Handle))
	<-make(chan bool)
}
```
> Why? Read this issue: https://github.com/golang/go/issues/29845


### Parameters and headers

- The parameters of the function are passed with a json payload as the body of a POST http request, you can get the parameters with the first item of `args []js.Value`: `args[0]`
- You can read the headers of the requests with the second item: `args[1]` 

### Compilation

#### If TinyGo
- Compile the **Go** file to wasm: 
```bash
tinygo build -no-debug  -o tinygo.add.wasm -target wasm ./main.go
```
🖐️ **Prefix the name of the wasm file by `tinygo.`**


#### If Go

```bash
GOOS=js GOARCH=wasm go build -o go.add.wasm ./main.go
```
🖐️ **Prefix the name of the wasm file by `go.`**


## Deploy a function

```bash
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
function_name="tinygo.add"
wasm_file="tinygo.add.wasm"
function_version="0.0.0"
curl -F "${function_name}=@${wasm_file}" \
  -H "Content-Type: multipart/form-data" \
  -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
  -X POST ${url_api}/functions/publish/${function_version}
```

- You can deploy several versions of a same function
- You nead to use an API token (initialized when starting the server thanks to the `ADMIN_GALAGO_TOKEN` environment variable)

🖐️ **Always prefix the name of the function by `tinygo.` or `go.`**

## Activate a function

You need to activate a version of the function to be able to call this function:

```bash
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
function_name="tinygo.add"
version_to_activate="0.0.0"
echo "🤖 activating ${version_to_activate} of ${function_name}"
url="${url_api}/functions/activate/${function_name}/${version_to_activate}"

curl -d "{}" \
  -H "Content-Type: application/json" \
  -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
  -X POST "${url}"
```

## Call a function

```bash
url_api="http://localhost:8080"
function_name="tinygo.add"
data='{"a":36, "b":6}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -X POST "${url_api}/functions/call/${function_name}"
```
> it always call the default activated version

## Call a specific version of a function

```bash
url_api="http://localhost:8080"
function_name="tinygo.add"
function_version="0.0.0"
data='{"a":36, "b":6}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -X POST "${url_api}/functions/call/${function_name}/${function_version}"
```

## Scale a version of a function

```bash
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
function_name="tinygo.add"
function_version="0.0.0"

curl \
  -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
  -X POST "${url_api}/functions/scale/${function_name}/${function_version}"
```

- Each scale call add a new independant process of the wasm function
- You can kill an individual process

## Get information about all functions, versions and processes

```bash
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
curl -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
     "${url_api}/functions/processes/data"
```

### Get information about one function

```bash
function_name="tinygo.hello"
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
curl -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
     "${url_api}/functions/processes/data/${function_name}"
```

> this § is in progress 🚧

## Kill a process of a version of a function

```bash
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
function_name="tinygo.helloworld"
function_version="0.0.3"
process_index=2

curl \
  -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
  -X POST "${url_api}/functions/kill/${function_name}/${function_version}/${process_index}"
```
